/*===========================================================================
  Copyright (C) 2016-2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.openxml;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Objects;
import java.util.stream.Collectors;

import static net.sf.okapi.filters.openxml.XMLEventHelpers.LOCAL_LEVEL;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.WPML_PARAGRAPH_STYLE;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.WPML_VAL;
import static net.sf.okapi.filters.openxml.XMLEventHelpers.getAttributeValue;

/**
 * Provides a block property markup component.
 */
class BlockProperties extends MarkupComponent implements Nameable {
    private final ConditionalParameters conditionalParameters;
    private final XMLEventFactory eventFactory;
    private final StartElement startElement;
    private final EndElement endElement;

    private final List<Attribute> attributes = new ArrayList<>();
    private final List<BlockProperty> properties = new ArrayList<>();
    private final SchemaDefinition.Component schemaDefinition;

    private final StrippableAttributes.General generalAttributes;

    BlockProperties(ConditionalParameters conditionalParameters, XMLEventFactory eventFactory, StartElement startElement, EndElement endElement, List<BlockProperty> properties) {
        this.conditionalParameters = conditionalParameters;
        this.eventFactory = eventFactory;
        this.startElement = startElement;
        this.endElement = endElement;

        Iterator iterator = startElement.getAttributes();

        while (iterator.hasNext()) {
            attributes.add((Attribute) iterator.next());
        }

        this.properties.addAll(properties);
        this.schemaDefinition = SchemaDefinitions.of(startElement);

        this.generalAttributes = new StrippableAttributes.General(conditionalParameters);
    }

    @Override
    public QName getName() {
        return startElement.getName();
    }

    @Override
    public List<XMLEvent> getEvents() {
        List<XMLEvent> events = new ArrayList<>();

        events.add(eventFactory.createStartElement(startElement.getName(), getAttributes().iterator(), startElement.getNamespaces()));

        for (BlockProperty property : properties) {
            events.addAll(property.getEvents());
        }
        events.add(endElement);

        return events;
    }

    List<Attribute> getAttributes() {
        return attributes;
    }

    List<BlockProperty> getProperties() {
        return properties;
    }

    boolean isEmpty() {
        return this.attributes.isEmpty() && this.properties.isEmpty();
    }

    String paragraphStyle() {
        Attribute paragraphLevelAttribute = paragraphLevelAttribute();

        if (null != paragraphLevelAttribute) {
            return paragraphLevelAttribute.getValue();
        }

        BlockProperty paragraphStyleProperty = paragraphStyleProperty();

        if (null != paragraphStyleProperty) {
            return getAttributeValue(paragraphStyleProperty.getEvents().get(0).asStartElement(), WPML_VAL);
        }

        return null;
    }

    private BlockProperty paragraphStyleProperty() {
        return blockProperty(WPML_PARAGRAPH_STYLE);
    }

    private BlockProperty blockProperty(final QName name) {
        for (BlockProperty property : properties) {
            if (property.getName().equals(name)) {
                return property;
            }
        }
        return null;
    }

    private Attribute paragraphLevelAttribute() {
        for (Attribute attribute : attributes) {
            if (LOCAL_LEVEL.equals(attribute.getName().getLocalPart())) {
                return attribute;
            }
        }

        return null;
    }

    void refine(final QName innerBlockPropertyName, final List<RunProperty> commonRunProperties) throws XMLStreamException {
        final ListIterator<BlockProperty> propertiesIterator = this.properties.listIterator();
        while (propertiesIterator.hasNext()) {
            final BlockProperty blockProperty = propertiesIterator.next();
            if (blockProperty.getName().equals(innerBlockPropertyName)) {
                updateProperty(propertiesIterator, blockProperty, commonRunProperties);
                return;
            }
        }
        addProperty(propertiesIterator, innerBlockPropertyName, commonRunProperties);
    }

    private void updateProperty(final ListIterator<BlockProperty> propertiesIterator,
                                final BlockProperty blockProperty, final List<RunProperty> commonRunProperties) throws XMLStreamException {
        final BlockProperties blockProperties = asBlockProperties(blockProperty);
        blockProperties.refineAll(commonRunProperties);
        propertiesIterator.set(
            BlockPropertyFactory.createBlockProperty(blockProperties.getEvents())
        );
    }

    private BlockProperties asBlockProperties(final BlockProperty blockProperty) throws XMLStreamException {
        final XMLEventReader eventReader = new XMLEventsReader(blockProperty.getEvents());
        final StartElement startElement = eventReader.nextEvent().asStartElement();
        final StartElementContext initialStartElementContext = StartElementContextFactory.createStartElementContext(
            startElement,
            this.startElement,
            eventReader,
            this.eventFactory,
            this.conditionalParameters,
            null
        );
        final StartElementContext startElementContext = StartElementContextFactory.createStartElementContext(
            generalAttributes.strip(initialStartElementContext),
            initialStartElementContext
        );
        final ElementSkipper elementSkipper = ElementSkipperFactory.createGeneralElementSkipper(
            this.conditionalParameters,
            ElementSkipper.RunPropertySkippableElement.RUN_PROPERTY_LANGUAGE,
            ElementSkipper.RevisionPropertySkippableElement.RUN_PROPERTY_INSERTED_PARAGRAPH_MARK,
            ElementSkipper.RevisionPropertySkippableElement.RUN_PROPERTY_DELETED_PARAGRAPH_MARK,
            ElementSkipper.RevisionPropertySkippableElement.PARAGRAPH_PROPERTIES_CHANGE,
            ElementSkipper.RevisionPropertySkippableElement.RUN_PROPERTIES_CHANGE
        );

        return MarkupComponentParser.parseBlockProperties(startElementContext, elementSkipper);
    }

    private void addProperty(final ListIterator<BlockProperty> propertiesIterator,
                             final QName innerBlockPropertyName, final List<RunProperty> commonRunProperties) {
        final List<Attribute> attributes = asAttributes(commonRunProperties);
        final List<RunProperty> properties = asProperties(commonRunProperties);
        final List<XMLEvent> events = new ArrayList<>();

        events.add(this.eventFactory.createStartElement(innerBlockPropertyName, attributes.iterator(), this.startElement.getNamespaces()));
        events.addAll(asXMLEvents(properties));
        events.add(this.eventFactory.createEndElement(innerBlockPropertyName, this.endElement.getNamespaces()));

        rewindToSchemaDefinedPlace(propertiesIterator, innerBlockPropertyName).add(
            BlockPropertyFactory.createBlockProperty(events)
        );
    }

    private List<Attribute> asAttributes(final List<RunProperty> commonRunProperties) {
        return commonRunProperties
            .stream()
            .filter(runProperty -> runProperty instanceof RunProperty.AttributeRunProperty)
            .map(runProperty -> this.eventFactory.createAttribute(runProperty.getName(), runProperty.getValue()))
            .collect(Collectors.toList());
    }

    private List<RunProperty> asProperties(final List<RunProperty> commonRunProperties) {
        return commonRunProperties
            .stream()
            .filter(runProperty -> !(runProperty instanceof RunProperty.AttributeRunProperty))
            .collect(Collectors.toList());
    }

    private static List<XMLEvent> asXMLEvents(final List<RunProperty> commonRunProperties) {
        return commonRunProperties
            .stream()
            .map(RunProperty::getEvents)
            .flatMap(List::stream)
            .collect(Collectors.toList());
    }

    /**
     * Rewinds the block properties iterator to a schema defined place.
     *
     * It is assumed that the iterator does not have next element
     * (we are at the end of the properties list).
     *
     * @param propertiesIterator     The properties iterator
     * @param innerBlockPropertyName The inner block property name
     *
     * @return The block properties iterator
     */
    private ListIterator<BlockProperty> rewindToSchemaDefinedPlace(
        final ListIterator<BlockProperty> propertiesIterator,
        final QName innerBlockPropertyName
    ) {
        if (!propertiesIterator.hasPrevious()) {
            // empty properties, just returning
            return propertiesIterator;
        }
        final Iterator<SchemaDefinition.Component> iterator =
            this.schemaDefinition.listIteratorAfter(innerBlockPropertyName);
        if (!iterator.hasNext()) {
            // the inner block property is the last component in the schema definition
            return propertiesIterator;
        }

        while (iterator.hasNext()) {
            final SchemaDefinition.Component component = iterator.next();
            while (propertiesIterator.hasPrevious()) {
                final BlockProperty blockProperty = propertiesIterator.previous();
                if (blockProperty.getName().equals(component.name())) {
                    return propertiesIterator;
                }
            }
            rewindToEndOfProperties(propertiesIterator);
        }

        // no block properties present after the inner block property
        return propertiesIterator;
    }

    private void rewindToEndOfProperties(final ListIterator<BlockProperty> propertiesIterator) {
        while (propertiesIterator.hasNext()) {
            propertiesIterator.next();
        }
    }

    private void refineAll(final List<RunProperty> commonRunProperties) {
        for (final RunProperty runProperty : commonRunProperties) {
            if (runProperty instanceof RunProperty.AttributeRunProperty) {
                updateOrAddAttribute((RunProperty.AttributeRunProperty) runProperty);
            } else {
                updateOrAddProperty(runProperty);
            }
        }
        alignWithSchemaDefinition();
    }

    private void alignWithSchemaDefinition() {
        if (this.properties.isEmpty()) {
            // there is nothing to align with
            return;
        }
        final List<BlockProperty> copiedProperties = new ArrayList<>(this.properties);
        this.properties.clear();
        final ListIterator<SchemaDefinition.Component> iterator = this.schemaDefinition.listIterator();
        while (iterator.hasNext() || !copiedProperties.isEmpty()) {
            final SchemaDefinition.Component component = iterator.next();
            switch (component.composition()) {
                case CHOICE:
                case SEQUENCE:
                case ALL:
                    findAndAppendMany(copiedProperties, component);
                    break;
                case NONE:
                    findAndAppendOne(copiedProperties, component);
            }
        }
    }

    private void findAndAppendMany(final List<BlockProperty> copiedProperties, final SchemaDefinition.Component component) {
        final Iterator<SchemaDefinition.Component> componentsIterator = component.listIterator();
        while (componentsIterator.hasNext()) {
            final SchemaDefinition.Component innerComponent = componentsIterator.next();
            findAndAppendOne(copiedProperties, innerComponent);
        }
    }

    private void findAndAppendOne(final List<BlockProperty> copiedProperties, final SchemaDefinition.Component component) {
        final Iterator<BlockProperty> copiedPropertiesIterator = copiedProperties.iterator();
        while (copiedPropertiesIterator.hasNext()) {
            final BlockProperty blockProperty = copiedPropertiesIterator.next();
            if (blockProperty.getName().equals(component.name())) {
                this.properties.add(blockProperty);
                copiedPropertiesIterator.remove();
                return;
            }
        }
    }

    private void updateOrAddAttribute(final RunProperty.AttributeRunProperty runProperty) {
        final ListIterator<Attribute> attributesIterator = this.attributes.listIterator();
        while (attributesIterator.hasNext()) {
            final Attribute attribute = attributesIterator.next();
            if (attribute.getName().equals(runProperty.getName())) {
                attributesIterator.set(
                    this.eventFactory.createAttribute(attribute.getName(), runProperty.getValue())
                );
                return;
            }
        }
        attributesIterator.add(
            this.eventFactory.createAttribute(runProperty.getName(), runProperty.getValue())
        );
    }

    private void updateOrAddProperty(final RunProperty runProperty) {
        final ListIterator<BlockProperty> propertiesIterator = this.properties.listIterator();
        while (propertiesIterator.hasNext()) {
            final BlockProperty blockProperty = propertiesIterator.next();
            if (blockProperty.getName().equals(runProperty.getName())) {
                propertiesIterator.set(
                    BlockPropertyFactory.createBlockProperty(runProperty.getEvents())
                );
                return;
            }
        }
        propertiesIterator.add(
            BlockPropertyFactory.createBlockProperty(runProperty.getEvents())
        );
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BlockProperties that = (BlockProperties) o;

        return Objects.equals(startElement.getName().getNamespaceURI(), that.startElement.getName().getNamespaceURI())
            && Objects.equals(startElement.getName().getLocalPart(), that.startElement.getName().getLocalPart())
            && Objects.equals(startElement.getName().getPrefix(), that.startElement.getName().getPrefix())
            && Objects.equals(endElement, that.endElement)
            && Objects.equals(attributes, that.attributes)
            && Objects.equals(properties, that.properties);
    }

    @Override
    public int hashCode() {
        return Objects.hash(
            startElement.getName().getNamespaceURI(),
            startElement.getName().getLocalPart(),
            startElement.getName().getPrefix(),
            endElement,
            attributes,
            properties
        );
    }

    @Override
    public String toString() {
        final StringBuilder builder = new StringBuilder();
        final String prefix = startElement.getName().getPrefix();
        if (!prefix.isEmpty()) {
            builder.append(prefix).append(":");
        }
        builder.append(startElement.getName().getLocalPart()).append(" ")
            .append("(").append(attributes.size()).append(")")
            .append(toString(attributes)).append(" ")
            .append("(").append(properties.size()).append(")")
            .append(properties);
        return builder.toString();
    }

    private String toString(final List<Attribute> attributes) {
        final StringBuilder builder = new StringBuilder();
        builder.append("[");
        for (final Attribute attribute : attributes) {
            final String prefix = attribute.getName().getPrefix();
            if (!prefix.isEmpty()) {
                builder.append(prefix).append(":");
            }
            builder.append(attribute.getName().getLocalPart()).append("=\"");
            builder.append(attribute.getValue()).append("\"");
        }
        builder.append("]");
        return builder.toString();
    }
}
