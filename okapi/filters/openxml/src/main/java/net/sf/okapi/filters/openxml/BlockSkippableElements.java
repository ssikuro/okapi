/*
 * =============================================================================
 *   Copyright (C) 2010-2019 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 * =============================================================================
 */

package net.sf.okapi.filters.openxml;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;

import static net.sf.okapi.filters.openxml.ElementSkipper.GeneralCrossStructureSkippableElement.BOOKMARK_END;
import static net.sf.okapi.filters.openxml.ElementSkipper.GeneralCrossStructureSkippableElement.BOOKMARK_START;
import static net.sf.okapi.filters.openxml.ElementSkipper.GeneralElementSkipper.isInsertedRunContentEndElement;
import static net.sf.okapi.filters.openxml.ElementSkipper.GeneralInlineSkippableElement.PROOFING_ERROR_ANCHOR;
import static net.sf.okapi.filters.openxml.ElementSkipper.RevisionInlineSkippableElement.RUN_DELETED_CONTENT;
import static net.sf.okapi.filters.openxml.StartElementContextFactory.createStartElementContext;

/**
 * Represents block skippable elements and provides a way to skip them.
 */
final class BlockSkippableElements {

    private final StartElementContext startElementContext;
    private final ElementSkipper insertedRunContentElementSkipper;
    private final ElementSkipper deletedRunContentAndProofingErrorElementSkipper;
    private final ElementSkipper bookmarkElementSkipper;

    BlockSkippableElements(StartElementContext startElementContext) {
        this.startElementContext = startElementContext;

        this.insertedRunContentElementSkipper = ElementSkipperFactory.createGeneralElementSkipper(
                this.startElementContext.getConditionalParameters(),
                ElementSkipper.RevisionInlineSkippableElement.RUN_INSERTED_CONTENT);

        this.deletedRunContentAndProofingErrorElementSkipper = ElementSkipperFactory.createGeneralElementSkipper(
                this.startElementContext.getConditionalParameters(),
                RUN_DELETED_CONTENT,
                PROOFING_ERROR_ANCHOR);

        this.bookmarkElementSkipper = ElementSkipperFactory.createBookmarkElementSkipper(
                BOOKMARK_START,
                BOOKMARK_END);
    }

    /**
     * Skips events according to the configured skippers.
     *
     * @param event An XML event
     *
     * @return {@code true}  - if an element has been skipped
     *         {@code false} - otherwise
     *
     * @throws XMLStreamException
     */
    boolean skip(final XMLEvent event) throws XMLStreamException {
        if (event.isStartElement() && this.insertedRunContentElementSkipper.canSkip(event.asStartElement(), this.startElementContext.getStartElement())) {
            this.insertedRunContentElementSkipper.skip(createStartElementContext(event.asStartElement(), this.startElementContext, ElementSkipper.InlineSkippableElement.class));
            return true;
        }
        if (isInsertedRunContentEndElement(event)) {
            return true;
        }
        if (event.isStartElement() && this.deletedRunContentAndProofingErrorElementSkipper.canSkip(event.asStartElement(), this.startElementContext.getStartElement())) {
            this.deletedRunContentAndProofingErrorElementSkipper.skip(createStartElementContext(event.asStartElement(), this.startElementContext));
            return true;
        }
        if (event.isStartElement() && this.bookmarkElementSkipper.canSkip(event.asStartElement(), null)) {
            this.bookmarkElementSkipper.skip(createStartElementContext(event.asStartElement(), this.startElementContext));
            return true;
        }
        return false;
    }
}
