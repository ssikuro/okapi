package net.sf.okapi.filters.openxml;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;
import java.util.Iterator;

import static net.sf.okapi.filters.openxml.XMLEventHelpers.isWhitespace;

/**
 * Provides an XML events reader.
 */
final class XMLEventsReader implements XMLEventReader {
    /**
     * An XML events iterator.
     */
    private final Iterator<XMLEvent> events;

    XMLEventsReader(final Iterable<XMLEvent> events) {
        this.events = events.iterator();
    }

    @Override public boolean hasNext() {
        return events.hasNext();
    }
    @Override public XMLEvent nextEvent() {
        return events.next();
    }
    @Override public void remove() {
        events.remove();
    }
    @Override public XMLEvent nextTag() throws XMLStreamException {
        for (XMLEvent e = nextEvent(); e != null; e = nextEvent()) {
            if (e.isStartElement() || e.isEndElement()) {
                return e;
            }
            else if (!isWhitespace(e)) {
                throw new IllegalStateException("Unexpected event: " + e);
            }
        }
        return null;
    }
    @Override public XMLEvent peek() throws XMLStreamException {
        throw new UnsupportedOperationException();
    }
    @Override public String getElementText() throws XMLStreamException {
        throw new UnsupportedOperationException();
    }
    @Override public Object getProperty(String name) throws IllegalArgumentException {
        throw new UnsupportedOperationException();
    }
    @Override public void close() throws XMLStreamException {
    }
    @Override public Object next() {
        return events.next();
    }
}
