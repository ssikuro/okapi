/*
 * =============================================================================
 *   Copyright (C) 2010-2019 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 * =============================================================================
 */
package net.sf.okapi.filters.openxml;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import java.util.ArrayList;
import java.util.List;

/**
 * Provides a style optimisation.
 */
interface StyleOptimisation {
    /**
     * Applies the style optimisation to the chunks of a block.
     *
     * @param chunks The chunks
     * @return The chunks with the optimised style
     */
    List<Chunk> applyTo(final List<Chunk> chunks) throws XMLStreamException;

    /**
     * Provides a bypass style optimisation.
     */
    final class Bypass implements StyleOptimisation {
        @Override
        public List<Chunk> applyTo(final List<Chunk> chunks) {
            return chunks;
        }
    }

    /**
     * Provides a default style optimisation.
     */
    final class Default implements StyleOptimisation {
        private final Bypass bypassOptimisation;
        private final ConditionalParameters conditionalParameters;
        private final XMLEventFactory eventFactory;
        private final QName blockPropertiesName;
        private final QName innerBlockPropertyName;

        Default(
            final Bypass bypassOptimisation,
            final ConditionalParameters conditionalParameters,
            final XMLEventFactory eventFactory,
            final QName blockPropertiesName,
            final QName innerBlockPropertyName
        ) {
            this.bypassOptimisation = bypassOptimisation;
            this.conditionalParameters = conditionalParameters;
            this.eventFactory = eventFactory;
            this.blockPropertiesName = blockPropertiesName;
            this.innerBlockPropertyName = innerBlockPropertyName;
        }

        @Override
        public List<Chunk> applyTo(final List<Chunk> chunks) throws XMLStreamException {
            if (chunks.size() <= 2) {
                // applying bypass as there is nothing to optimise (an empty block)
                return bypassOptimisation.applyTo(chunks);
            }

            final List<Chunk> innerChunks = chunks.subList(1, chunks.size() - 1);
            final List<RunProperty> commonRunProperties = commonRunPropertiesOf(innerChunks);

            if (commonRunProperties.isEmpty()) {
                // there is nothing to optimise (the run properties are all different)
                return bypassOptimisation.applyTo(chunks);
            }

            final Block.BlockMarkup firstMarkup = (Block.BlockMarkup) chunks.get(0);
            final BlockProperties blockProperties = blockPropertiesOf(firstMarkup);
            blockProperties.refine(this.innerBlockPropertyName, commonRunProperties);
            firstMarkup.updateOrAddComponent(blockProperties);
            refineRuns(innerChunks, commonRunProperties);

            return chunks;
        }

        /**
         * Obtains {@link BlockProperties} out of {@link Block.BlockMarkup}.
         *
         * @param markup The markup
         * @return The obtained block properties or new empty block properties
         */
        private BlockProperties blockPropertiesOf(final Block.BlockMarkup markup) {
            final BlockProperties blockProperties = markup.blockProperties();
            if (null == blockProperties) {
                final StartElement startElement = eventFactory.createStartElement(
                    this.blockPropertiesName.getPrefix(),
                    this.blockPropertiesName.getNamespaceURI(),
                    this.blockPropertiesName.getLocalPart()
                );
                final EndElement endElement = eventFactory.createEndElement(
                    this.blockPropertiesName.getPrefix(),
                    this.blockPropertiesName.getNamespaceURI(),
                    this.blockPropertiesName.getLocalPart()
                );
                return BlockPropertiesFactory.createBlockProperties(
                    this.conditionalParameters,
                    this.eventFactory,
                    startElement,
                    endElement,
                    new ArrayList<>()
                );
            }
            return blockProperties;
        }

        private static List<RunProperty> commonRunPropertiesOf(final List<Chunk> chunks) {
            final List<RunProperty> commonRunProperties = new ArrayList<>();
            for (final Chunk chunk : chunks) {
                // care about runs only
                if (chunk instanceof Run) {
                    if (commonRunProperties.isEmpty()) {
                        commonRunProperties.addAll(((Run) chunk).getProperties().getProperties());
                    } else {
                        commonRunProperties.retainAll(((Run) chunk).getProperties().getProperties());
                    }
                }
            }
            return commonRunProperties;
        }

        private void refineRuns(final List<Chunk> chunks, final List<RunProperty> commonRunProperties) {
            for (final Chunk chunk : chunks) {
                if (chunk instanceof Run) {
                    ((Run) chunk).refineRunProperties(commonRunProperties);
                }
            }
        }
    }
}
