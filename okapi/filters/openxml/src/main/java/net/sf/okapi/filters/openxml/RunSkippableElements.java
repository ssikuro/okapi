/*
 * =============================================================================
 *   Copyright (C) 2010-2019 by the Okapi Framework contributors
 * -----------------------------------------------------------------------------
 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 * =============================================================================
 */

package net.sf.okapi.filters.openxml;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;

import static net.sf.okapi.filters.openxml.ElementSkipper.RevisionPropertySkippableElement.RUN_PROPERTIES_CHANGE;
import static net.sf.okapi.filters.openxml.ElementSkipper.RunPropertySkippableElement.RUN_PROPERTY_LANGUAGE;
import static net.sf.okapi.filters.openxml.ElementSkipper.RunPropertySkippableElement.RUN_PROPERTY_NO_SPELLING_OR_GRAMMAR;
import static net.sf.okapi.filters.openxml.StartElementContextFactory.createStartElementContext;

final class RunSkippableElements {

    private final StartElementContext startElementContext;
    private final ElementSkipper lastRenderedPageBreakElementSkipper;
    private final ElementSkipper softHyphenElementSkipper;
    private final ElementSkipper alternateContentFallbackElementSkipper;
    private final ElementSkipper runPropertiesSkipper;

    RunSkippableElements(StartElementContext startElementContext) {
        this.startElementContext = startElementContext;

        this.lastRenderedPageBreakElementSkipper = ElementSkipperFactory.createGeneralElementSkipper(
                startElementContext.getConditionalParameters(),
                ElementSkipper.GeneralInlineSkippableElement.LAST_RENDERED_PAGE_BREAK);

        this.softHyphenElementSkipper = ElementSkipperFactory.createGeneralElementSkipper(
                startElementContext.getConditionalParameters(),
                ElementSkipper.GeneralInlineSkippableElement.SOFT_HYPHEN);

        this.alternateContentFallbackElementSkipper = ElementSkipperFactory.createGeneralElementSkipper(
                startElementContext.getConditionalParameters(),
                ElementSkipper.GeneralInlineSkippableElement.ALTERNATE_CONTENT_FALLBACK);

        this.runPropertiesSkipper = ElementSkipperFactory.createGeneralElementSkipper(startElementContext.getConditionalParameters(),
                RUN_PROPERTIES_CHANGE,
                RUN_PROPERTY_LANGUAGE,
                RUN_PROPERTY_NO_SPELLING_OR_GRAMMAR);

    }

    boolean skip(final XMLEvent e) throws XMLStreamException {
        if (e.isStartElement() && lastRenderedPageBreakElementSkipper.canSkip(e.asStartElement(), startElementContext.getStartElement())) {
            lastRenderedPageBreakElementSkipper.skip(createStartElementContext(e.asStartElement(), startElementContext.getEventReader(), null, startElementContext.getConditionalParameters()));
            return true;
        } else if (startElementContext.getConditionalParameters().getIgnoreSoftHyphenTag()
                && e.isStartElement() && softHyphenElementSkipper.canSkip(e.asStartElement(), startElementContext.getStartElement())) {
            // Ignore soft hyphens
            softHyphenElementSkipper.skip(createStartElementContext(e.asStartElement(), startElementContext.getEventReader(), null, startElementContext.getConditionalParameters()));
            return true;
        } else if (e.isStartElement() && alternateContentFallbackElementSkipper.canSkip(e.asStartElement(), startElementContext.getStartElement())) {
            alternateContentFallbackElementSkipper.skip(createStartElementContext(e.asStartElement(), startElementContext.getEventReader(), null, startElementContext.getConditionalParameters()));
            return true;
        } else if (skipProperties(e, startElementContext)) {
            return true;
        }
        return false;
    }

    boolean skipProperties(final XMLEvent e, final StartElementContext startElementContext) throws XMLStreamException {
        if (e.isStartElement() && runPropertiesSkipper.canSkip(e.asStartElement(), startElementContext.getStartElement())) {
            runPropertiesSkipper.skip(createStartElementContext(e.asStartElement(), startElementContext));
            return true;
        }
        return false;
    }
}
