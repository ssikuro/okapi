package net.sf.okapi.connectors.microsoft;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.*;

import net.sf.okapi.common.FileLocation;
import net.sf.okapi.common.StreamUtil;

import java.util.List;

@Deprecated // This is for API v2 which retires on 2019-4-30
@RunWith(JUnit4.class)
public class TestGetTranslationResponseParser {

	private GetTranslationsResponseParser parser;
	private FileLocation root;

	@Before
	public void setup() {
		root = FileLocation.fromClass(getClass());
		parser = new GetTranslationsResponseParser();
	}

	private static final String EXPECTED_TRANSLATION =
			"<br id='p1'>Chats et<br id='p2'>chiens<br id='p3'>&amp;<br id='p4'>mouffettes<br id='p5'><br id='p6'>";
	@Test
	public void testParseGetTranslationsResponse() throws Exception {
		String xml = StreamUtil.streamUtf8AsString(
				root.in("/GetTranslations-Response-Coded.xml").asInputStream());
		List<TranslationResponse> responses = parser.parseGetTranslationsResponse(xml, 10, 50);
		assertEquals(1, responses.size());
		TranslationResponse r = responses.get(0);
		assertNull(r.sourceText);
		assertEquals(EXPECTED_TRANSLATION, r.translatedText);
		assertEquals(5, r.rating);
		assertEquals(100, r.matchDegree);
	}

	@Test
	public void testParseGetTranslationsArrayResponse() throws Exception {
		String xml = StreamUtil.streamUtf8AsString(
				root.in("/GetTranslationsArray-Response.xml").asInputStream());
		List<List<TranslationResponse>> responses = parser.parseGetTranslationsArrayResponse(xml, 10, 50);
		assertEquals(3, responses.size());
		List<TranslationResponse> r = responses.get(0);
		assertNull(r.get(0).sourceText);
		assertEquals("Cuando él siente casi.", r.get(0).translatedText);
		assertEquals(5, r.get(0).rating);
		assertEquals(100, r.get(0).matchDegree);
		// Cuando él salto que volar casi.
		// Él no consiguió ningún sentido apenas.
	}
}
